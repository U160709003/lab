
public class Int2Bin {

	public static void main(String[] args) {
		int number = Integer.parseInt(args[0]);
		System.out.println(Int2Bin(number));
	}	
	private static String Int2Bin(int number){	
		if ((number == 0) || (number==1)) 
			return number + "";
		
		return Int2Bin(number/2) + (number %2 );
			
	}
		
	
}	
