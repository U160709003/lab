package stack;

import stack.Stack;
import stack.StackArrayListImpl;

public class Test {
	public static void main(String[] args) {
		
		Stack<String> stack = new StackArrayListImpl<>();
		
		stack.push("Hello");
		stack.push("World");
		stack.push("ABC");
		//Stack.push(8);
		//Stack.push(new Double(9,5));
		
		System.out.println(stack.toList());
		
		if(!stack.empty()) {
			String str = stack.pop();
			System.out.println(str);
		}
		if(!stack.empty()) {
			System.out.println(stack.pop());
		}
		if(!stack.empty()) {
			System.out.println(stack.pop());
		}

		
		if(!stack.empty())
			System.out.println(stack.pop());
		if(!stack.empty())
			System.out.println(stack.pop());
		if(!stack.empty())
			System.out.println(stack.pop());
		

	}

}
